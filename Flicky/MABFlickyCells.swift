//
//  MABFlickyCollectionViewCell.swift
//  Flicky
//
//  Created by Muhammad Bassio on 2/2/15.
//  Copyright (c) 2015 Muhammad Bassio. All rights reserved.
//

import UIKit

class MABFlickyCollectionViewCell: UICollectionViewCell {
  
  var imageView:UIImageView = UIImageView(frame: CGRectZero)
  var currentURL:String = ""
  var indexNumber:Int = 0
  var loading:MABActivityIndicatorView = MABActivityIndicatorView(frame: CGRectMake(0, 0, 44, 44))
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    self.backgroundColor = UIColor.lightGrayColor().colorWithAlphaComponent(0.3)
    self.addSubview(self.imageView)
    self.addSubview(self.loading)
  }

  required init(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }
  
  override func layoutSubviews() {
    self.imageView.frame = self.bounds
    self.loading.center = CGPointMake(self.bounds.size.width / 2, self.bounds.size.height / 2)
  }
  
  func loadPhotoWithURL(url:String, andIndex index:Int) {
    
    self.imageView.image = nil
    self.loading.startAnimating()
    MABImageCache.sharedCache.cancelImageForURL(self.currentURL)
    self.currentURL = url
    self.indexNumber = index
    MABImageCache.sharedCache.loadImageForURL(url, completionHandler: {(image:UIImage) in
      self.loading.stopAnimating()
      self.imageView.image = image
      self.imageView.contentMode = UIViewContentMode.ScaleAspectFit
    })
    
  }
  
}

class MABFlickyGalleryCell: UICollectionViewCell {
  
  var imageView:UIImageView = UIImageView(frame: CGRectZero)
  var currentURL:String = ""
  var loading:MABActivityIndicatorView = MABActivityIndicatorView(frame: CGRectMake(0, 0, 44, 44))
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    self.backgroundColor = UIColor.blackColor()
    self.loading.color = UIColor.whiteColor()
    self.addSubview(self.imageView)
    self.addSubview(self.loading)
  }
  
  required init(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }
  
  override func layoutSubviews() {
    self.imageView.frame = self.bounds
    self.loading.center = CGPointMake(self.bounds.size.width / 2, self.bounds.size.height / 2)
  }
  
  func loadPhotoWithURL(url:String, placeholderImage:UIImage, andIndex index:Int) {
    
    self.imageView.image = placeholderImage
    self.imageView.contentMode = UIViewContentMode.ScaleAspectFit
    MABImageCache.sharedCache.cancelImageForURL(self.currentURL)
    self.currentURL = url
    MABImageCache.sharedCache.loadImageForURL(url, completionHandler: {(image:UIImage) in
      self.imageView.image = image
      self.imageView.contentMode = UIViewContentMode.ScaleAspectFit
    })
    
  }
  
  func loadPhotoWithURL(url:String, andIndex index:Int) {
    
    self.imageView.image = nil
    MABImageCache.sharedCache.cancelImageForURL(self.currentURL)
    self.currentURL = url
    self.loading.startAnimating()
    MABImageCache.sharedCache.loadImageForURL(url, completionHandler: {(image:UIImage) in
      self.loading.stopAnimating()
      self.imageView.image = image
      self.imageView.contentMode = UIViewContentMode.ScaleAspectFit
    })
    
  }
  
}
