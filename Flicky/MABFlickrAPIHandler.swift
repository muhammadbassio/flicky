//
//  MABFlickrAPIHandler.swift
//  Flicky
//
//  Created by Muhammad Bassio on 2/1/15.
//  Copyright (c) 2015 Muhammad Bassio. All rights reserved.
//

import UIKit

class MABFlickrAPIHelper {
  
  var thmbSize:String = "_n"
  var imageSize:String = "_b"
  
  class var sharedHelper : MABFlickrAPIHelper {
    struct Static {
      static var onceToken : dispatch_once_t = 0
      static var instance : MABFlickrAPIHelper? = nil
    }
    dispatch_once(&Static.onceToken) {
      Static.instance = MABFlickrAPIHelper()
    }
    return Static.instance!
  }
  
  init() {
    
    if (UIDevice.currentDevice().userInterfaceIdiom == UIUserInterfaceIdiom.Pad) {
      if (UIScreen.mainScreen().scale > 1) {
        self.thmbSize = ""
        self.imageSize = "_k"
      }
    }
    else {
      if (UIScreen.mainScreen().scale > 2) {
        self.thmbSize = ""
        self.imageSize = "_k"
      }
    }
    
  }
  
}


class MABFlickrPhotoResult {
  
  var identifier:String = ""
  var owner:String = ""
  var secret:String = ""
  var server:String = ""
  var title:String = ""
  var farm:Int = 0
  var ispublic:Int = 1
  var isfriend:Int = 0
  var isfamily:Int = 0
  
  var thmbURL = "https://farm9.staticflickr.com/0/0_0_n.jpg"
  var imageURL = "https://s.yimg.com/pw/images/en-us/photo_unavailable_n.png"
  
  
  
  
  init() {
    
  }
  
  init(dict: JSON) {
    
    if let pid = dict["id"].string {
      self.identifier = dict["id"].stringValue;
    }
    if let powner = dict["owner"].string {
      self.owner = dict["owner"].stringValue;
    }
    if let psecret = dict["secret"].string {
      self.secret = dict["secret"].stringValue;
    }
    if let pserver = dict["server"].string {
      self.server = dict["server"].stringValue;
    }
    if let ptitle = dict["title"].string {
      self.title = dict["title"].stringValue;
    }
    
    
    if let pfarm = dict["farm"].int {
      self.farm = dict["farm"].intValue;
    }
    if let pispublic = dict["ispublic"].int {
      self.ispublic = dict["ispublic"].intValue;
    }
    if let pisfriend = dict["isfriend"].int {
      self.isfriend = dict["isfriend"].intValue;
    }
    if let pisfamily = dict["isfamily"].int {
      self.isfamily = dict["isfamily"].intValue;
    }
    
    self.thmbURL = "https://farm\(self.farm).staticflickr.com/\(self.server)/\(self.identifier)_\(self.secret)\(MABFlickrAPIHelper.sharedHelper.thmbSize).jpg"
    self.imageURL = "https://farm\(self.farm).staticflickr.com/\(self.server)/\(self.identifier)_\(self.secret)\(MABFlickrAPIHelper.sharedHelper.imageSize).jpg"
    
  }
  
  
  
  
}


class MABFlickrAPIHandler: NSObject {
  
  let apiKey = "1d7ab632acb05e1807ede656598ee218"
  
  class var sharedHandler : MABFlickrAPIHandler {
    struct Static {
      static var onceToken : dispatch_once_t = 0
      static var instance : MABFlickrAPIHandler? = nil
    }
    dispatch_once(&Static.onceToken) {
      Static.instance = MABFlickrAPIHandler()
    }
    return Static.instance!
  }
  
  
  // Search for text
  // text: string to search for
  // page: page number, starts with 1
  // completion: block to handle fetched results
  
  func searchPhotosContaining(text:String, perPage:Int,page:Int,completion:(resultsArray:[MABFlickrPhotoResult], err:NSError!) -> Void) {
    
    var requestURL:String = "https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=\(self.apiKey)&text=\(text)&per_page=\(perPage)&page=\(page)&format=json&nojsoncallback=1"
    
    println("\(requestURL)")
    var request = NSMutableURLRequest(URL: NSURL(string:requestURL.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)!)!)
    request.HTTPMethod = "GET";
    
    var session = NSURLSession.sharedSession()
    var task = session.dataTaskWithRequest(request, completionHandler: {data2, response, error -> Void in
      
      var rArray:[MABFlickrPhotoResult] = []
      var rError:NSError! = nil
      var json = JSON(data: data2)
      
      if (error == nil) {
        if let stat = json["stat"].string {
          let status = json["stat"].stringValue
          if (status == "ok") {
            if let itmsc = json["photos"]["photo"].array {
              let itms = json["photos"]["photo"]
              for (index: String, subJson: JSON) in itms {
                var itm = itms[index.toInt()!]
                var res = MABFlickrPhotoResult(dict: itm)
                rArray.append(res);
              }
            }
            else {
              rError = NSError(domain: "MABFlickrAPIHandler", code: 1001, userInfo: [NSLocalizedDescriptionKey: "Failed to parse JSON"]);
            }
          }
          else {
            var rCode = 0
            var rMessage = "Response error"
            if let msg = json["message"].string {
              rMessage = json["message"].stringValue
            }
            if let cd = json["code"].int {
              rCode = json["code"].intValue
            }
            rError = NSError(domain: "MABFlickrAPIHandler", code: rCode, userInfo: [NSLocalizedDescriptionKey: rMessage]);
          }
        }
        else {
          rError = NSError(domain: "MABFlickrAPIHandler", code: 1001, userInfo: [NSLocalizedDescriptionKey: "Failed to parse JSON"]);
        }
      }
      else {
        rError = error
      }
      
      dispatch_async(dispatch_get_main_queue()) {
        completion(resultsArray: rArray, err: rError)
      }
      
    });
    task.resume();
    
  }
  
  
  
  
}
