//
//  MABFlickyCollectionView.swift
//  Flicky
//
//  Created by Muhammad Bassio on 2/2/15.
//  Copyright (c) 2015 Muhammad Bassio. All rights reserved.
//

import UIKit
import Foundation

class MABFlickyCollectionView: UICollectionView, UICollectionViewDataSource, UICollectionViewDelegate {
  
  var collectionDelegate:UICollectionViewDelegate!
  var collectionDataSource:UICollectionViewDataSource!
  var lastScrollPosition:CGPoint = CGPointMake(0, 0)
  var currentScrollPosition:CGPoint = CGPointMake(0, 0)
  
  override init(frame: CGRect, collectionViewLayout layout: UICollectionViewLayout) {
    super.init(frame: frame, collectionViewLayout: layout)
    self.delegate = self
    self.dataSource = self
  }

  required init(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }
  
  
  func animate(layer:CALayer, speed:Float) -> NSTimeInterval {
    if (speed != 0.0) {
      layer.transform = CATransform3DMakeScale(0.8, 0.8, 0.8);
      layer.opacity = 1.0 - fabs(speed);
    }
    return 2 * 0.4;
  }
  
  
  func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
    
    var cell:UICollectionViewCell! = nil
    cell = self.collectionDataSource?.collectionView(collectionView, cellForItemAtIndexPath: indexPath)
    
    //self.animate(cell.layer, speed: normalizedSpeed)
    
    cell?.layer.transform = CATransform3DIdentity
    cell?.layer.transform = CATransform3DMakeScale(0.8, 0.8, 0.8)
    cell?.layer.opacity = 0.0
    
    UIView.animateWithDuration(0.8, animations: {
      cell?.layer.transform = CATransform3DMakeScale(1.0, 1.0, 1.0)
      cell?.layer.opacity = 1.0
    })
    
    /*if (_transformBlock && shouldAnimate) {
      NSTimeInterval animationDuration = _transformBlock(cell.layer, normalizedSpeed);
      // The block-based equivalent doesn't play well with iOS 4
      [UIView beginAnimations:nil context:NULL];
      [UIView setAnimationDuration:animationDuration];
    }*/
    
    return cell
    
  }
  
  func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return self.collectionDataSource.collectionView(collectionView, numberOfItemsInSection: section)
  }
  
  func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
    self.collectionDelegate.collectionView!(collectionView, didSelectItemAtIndexPath: indexPath)
  }
  
  func scrollViewDidScroll(scrollView: UIScrollView) {
    self.collectionDelegate.scrollViewDidScroll!(scrollView)
    self.lastScrollPosition = self.currentScrollPosition
    self.currentScrollPosition = scrollView.contentOffset
  }
  
  
}
