//
//  MABFlickyLayout.swift
//  Flicky
//
//  Created by Muhammad Bassio on 2/2/15.
//  Copyright (c) 2015 Muhammad Bassio. All rights reserved.
//

import UIKit

class MABFlickyLayout: UICollectionViewLayout {
  
  var layoutInfo:NSDictionary = NSDictionary()
  var itemInsets:UIEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
  var itemSize:CGSize = CGSize(width: 0, height: 0)
  var interItemSpacingY:CGFloat = 0
  var topheight:CGFloat = 0
  var numberOfColumns:Int = 1
  var isPad:Bool = false
  
  override init() {
    super.init()
    setup()
  }
  
  required init(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setup()
  }
  
  
  func setup() {
    self.itemInsets = UIEdgeInsetsMake(6.0, 8.0, 8.0, 8.0)
    self.interItemSpacingY = 8.0
    self.topheight = 64
    self.numberOfColumns = 2
    if (UIDevice.currentDevice().userInterfaceIdiom == UIUserInterfaceIdiom.Pad) {
      self.isPad = true
    }
  }
  
  override func prepareLayout() {
    var cellLayoutInfo:NSMutableDictionary = NSMutableDictionary()
    if let sectionCount = self.collectionView?.numberOfSections() {
      for var section = 0; section < sectionCount; section++ {
        if let itemCount = self.collectionView?.numberOfItemsInSection(section) {
          for var item = 0; item < itemCount; item++ {
            var indexPath = NSIndexPath(forItem: item, inSection: section)
            var itemAttributes = UICollectionViewLayoutAttributes(forCellWithIndexPath: indexPath)
            itemAttributes.frame = self.frameForThumbAtIndexPath(indexPath)
            cellLayoutInfo[indexPath] = itemAttributes
          }
        }
      }
    }
    self.layoutInfo = cellLayoutInfo
  }
  
  
  func frameForThumbAtIndexPath(indexpath:NSIndexPath) -> CGRect {
    
    if (self.isPad) {
      if let w = self.collectionView?.frame.size.width {
        self.itemSize = CGSizeMake((w - 48) / 3, (w - 48) / 3)
        self.itemInsets = UIEdgeInsetsMake(10.0, 12.0, 12.0, 12.0)
        self.numberOfColumns = 3
        self.interItemSpacingY = 12
        if (UIInterfaceOrientationIsLandscape(UIApplication.sharedApplication().statusBarOrientation)) {
          self.itemSize = CGSizeMake((w - 60) / 4, (w - 60) / 4)
          self.numberOfColumns = 4
        }
      }
    }
    else {
      if let w = self.collectionView?.frame.size.width {
        self.itemSize = CGSizeMake((w - 24) / 2, (w - 24) / 2)
        self.itemInsets = UIEdgeInsetsMake(6.0, 8.0, 8.0, 8.0)
        self.numberOfColumns = 2
        self.interItemSpacingY = 8
      }
    }
    
    var row = indexpath.row / self.numberOfColumns
    var column = indexpath.row % self.numberOfColumns
    
    var originX = floorf(Float(self.itemInsets.left + ((self.itemSize.width + self.interItemSpacingY) * CGFloat(column))))
    var originY = floor(Float(self.itemInsets.top + self.topheight + ((self.itemSize.height + self.interItemSpacingY) * CGFloat(row))))
    return CGRectMake(CGFloat(originX), CGFloat(originY), self.itemSize.width, self.itemSize.height)
    
  }
  
  override func layoutAttributesForElementsInRect(rect: CGRect) -> [AnyObject]? {
    var allAttributes:[UICollectionViewLayoutAttributes] = []
    layoutInfo.enumerateKeysAndObjectsUsingBlock({(indexPath, attributes, innerStop) -> Void in
      if (CGRectIntersectsRect(rect, attributes.frame)) {
        allAttributes.append(attributes as UICollectionViewLayoutAttributes)
      }
    })
    return allAttributes
  }
  
  override func layoutAttributesForItemAtIndexPath(indexPath: NSIndexPath) -> UICollectionViewLayoutAttributes! {
    return self.layoutInfo[indexPath] as UICollectionViewLayoutAttributes
  }
  
  override func collectionViewContentSize() -> CGSize {
    
    var width:CGFloat = 0
    var height:CGFloat = 0
    
    if let w = self.collectionView?.bounds.size.width {
      width = w
    }
    else {
      width = 0
    }
    
    if let ic = self.collectionView?.numberOfItemsInSection(0) {
      var itemsCount:Int = ic
      var rowCount:Int = itemsCount / self.numberOfColumns
      if ((itemsCount % self.numberOfColumns) > 0) {
        rowCount++
      }
      height = self.itemInsets.top + self.topheight + (CGFloat(rowCount) * self.itemSize.height) + (CGFloat(rowCount - 1) * self.interItemSpacingY) + self.itemInsets.bottom + 44
      
    }
    
    return CGSizeMake(width, height)
  }
  
}

class MABFlickyGalleryLayout: UICollectionViewLayout {
  
  var layoutInfo:NSDictionary = NSDictionary()
  var itemSize:CGSize = CGSize(width: 0, height: 0)
  
  override init() {
    super.init()
  }
  
  required init(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }
  
  override func prepareLayout() {
    var cellLayoutInfo:NSMutableDictionary = NSMutableDictionary()
    if let sectionCount = self.collectionView?.numberOfSections() {
      for var section = 0; section < sectionCount; section++ {
        if let itemCount = self.collectionView?.numberOfItemsInSection(section) {
          for var item = 0; item < itemCount; item++ {
            var indexPath = NSIndexPath(forItem: item, inSection: section)
            var itemAttributes = UICollectionViewLayoutAttributes(forCellWithIndexPath: indexPath)
            itemAttributes.frame = self.frameForThumbAtIndexPath(indexPath)
            cellLayoutInfo[indexPath] = itemAttributes
          }
        }
      }
    }
    self.layoutInfo = cellLayoutInfo
  }
  
  
  func frameForThumbAtIndexPath(indexpath:NSIndexPath) -> CGRect {
    
    if let w = self.collectionView?.frame.size.width {
      if let h = self.collectionView?.frame.size.height {
        self.itemSize = CGSizeMake(w, h)
      }
    }
    var originX:CGFloat = self.itemSize.width * CGFloat(indexpath.row)
    return CGRectMake(originX, 0, self.itemSize.width, self.itemSize.height)
    
  }
  
  override func layoutAttributesForElementsInRect(rect: CGRect) -> [AnyObject]? {
    var allAttributes:[UICollectionViewLayoutAttributes] = []
    layoutInfo.enumerateKeysAndObjectsUsingBlock({(indexPath, attributes, innerStop) -> Void in
      if (CGRectIntersectsRect(rect, attributes.frame)) {
        allAttributes.append(attributes as UICollectionViewLayoutAttributes)
      }
    })
    return allAttributes
  }
  
  override func layoutAttributesForItemAtIndexPath(indexPath: NSIndexPath) -> UICollectionViewLayoutAttributes! {
    return self.layoutInfo[indexPath] as UICollectionViewLayoutAttributes
  }
  
  override func collectionViewContentSize() -> CGSize {
    
    var width:CGFloat = 0
    var height:CGFloat = 0
    
    if let w = self.collectionView?.bounds.size.width {
      if let h = self.collectionView?.bounds.size.height {
        height = h
        if let ic = self.collectionView?.numberOfItemsInSection(0) {
          width = CGFloat(ic) * CGFloat(w)
        }
      }
    }
    
    return CGSizeMake(width, height)
  }
  
}
