//
//  MABImageCache.swift
//  Tuber
//
//  Created by Muhammad Bassio on 12/25/14.
//  Copyright (c) 2014 Muhammad Bassio. All rights reserved.
//

import Foundation
import UIKit


class MABImageCache {
  
  var cancelledImages:[String] = [];
  var cachePath:NSString = "";
  var ioQueue:dispatch_queue_t;
  
  class var sharedCache : MABImageCache {
    struct Static {
      static var onceToken : dispatch_once_t = 0
      static var instance : MABImageCache? = nil
    }
    dispatch_once(&Static.onceToken) {
      Static.instance = MABImageCache()
    }
    return Static.instance!
  }
  
  init() {
    let paths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory,.UserDomainMask,true)[0] as NSString
    cachePath = paths.stringByAppendingPathComponent("com.muhammadbassio.Image.cache")
    if (!NSFileManager.defaultManager().fileExistsAtPath(cachePath)) {
      var error:NSError?;
      NSFileManager.defaultManager().createDirectoryAtPath(cachePath, withIntermediateDirectories: false, attributes: nil, error: &error)
      println("\(error.debugDescription)");
    }
    else {
      println("directory exists")
    }
    ioQueue = dispatch_queue_create("com.muhammadbassio.Image.cache", DISPATCH_QUEUE_SERIAL);
  }
  
  func getSize() -> UInt64 {
    var size:UInt64 = 0;
    var fileEnumerator = NSFileManager.defaultManager().enumeratorAtPath(cachePath)!
    while let fileName = fileEnumerator.nextObject() as? String {
      let filePath = cachePath.stringByAppendingPathComponent(fileName)
      let attrs:NSDictionary = NSFileManager.defaultManager().attributesOfItemAtPath(filePath, error: nil)!
      size = size + attrs.fileSize()
    }
    return size;
  }
  
  func isCached(url:String) -> Bool {
    
    if let encoded = url.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding) {
      if (NSFileManager.defaultManager().fileExistsAtPath(cachePath.stringByAppendingPathComponent(url.stringByAddingPercentEncodingWithAllowedCharacters(.URLHostAllowedCharacterSet())!))) {
        return true;
      }
    }
    return false
  }
  
  func clearDiskWithCompletionHandler(completionHandler:(success:Bool) -> Void) {
    dispatch_async(ioQueue, {
      NSFileManager.defaultManager().removeItemAtPath(self.cachePath, error: nil)
      NSFileManager.defaultManager().createDirectoryAtPath(self.cachePath, withIntermediateDirectories: false, attributes: nil, error: nil)
      dispatch_async(dispatch_get_main_queue(), {
        completionHandler(success: true);
      });
    });
  }
  
  func loadImageForURL(url:String, completionHandler:(image:UIImage) -> Void) {
    
    if (self.isCached(url)) {
      if let data:NSData = NSData(contentsOfFile: cachePath.stringByAppendingPathComponent(url.stringByAddingPercentEncodingWithAllowedCharacters(.URLHostAllowedCharacterSet())!)) {
        if let img:UIImage = UIImage(data: data) {
          println("found in cache")
          completionHandler(image: img)
        }
      }
    }
    else {
      
      for var i = 0; i < self.cancelledImages.count; i++ {
        if (self.cancelledImages[i] == url) {
          self.cancelledImages.removeAtIndex(i)
          break
        }
      }
      
      var imageUrl = NSURL(string: url)
      var request = NSURLRequest(URL: imageUrl!)
      var requestQueue : NSOperationQueue = NSOperationQueue()
      NSURLConnection.sendAsynchronousRequest(request, queue: requestQueue, completionHandler:
        {(response: NSURLResponse!, responseData: NSData!, error: NSError!) -> Void in
          if error != nil {
            println("error")
          }
          else {
            var cancelled = false
            for var i = 0; i < self.cancelledImages.count; i++ {
              if (self.cancelledImages[i] == url) {
                cancelled = true
                break
              }
            }
            if var img:UIImage = UIImage(data: responseData) {
              UIImageJPEGRepresentation(img, 100).writeToFile(self.cachePath.stringByAppendingPathComponent(url.stringByAddingPercentEncodingWithAllowedCharacters(.URLHostAllowedCharacterSet())!), atomically: true)
              if(!cancelled) {
                dispatch_async(dispatch_get_main_queue()) {
                  completionHandler(image: img)
                }
              }
            }
          }
      })
    }
  }
  
  func cancelImageForURL(url:String) {
    var found = false
    for var i = 0; i < self.cancelledImages.count; i++ {
      if (self.cancelledImages[i] == url) {
        found = true
        break;
      }
    }
    if (!found) {
      self.cancelledImages.append(url)
    }
    
  }
  
  
}
