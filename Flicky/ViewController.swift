//
//  ViewController.swift
//  Flicky
//
//  Created by Muhammad Bassio on 2/1/15.
//  Copyright (c) 2015 Muhammad Bassio. All rights reserved.
//

import UIKit


class ViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UISearchBarDelegate, UIViewControllerTransitioningDelegate  {
  
  var searchView:UINavigationBar = UINavigationBar(frame: CGRectZero)
  var searchBar:UISearchBar = UISearchBar(frame: CGRectZero)
  var loading:MABActivityIndicatorView = MABActivityIndicatorView(frame: CGRectMake(0, 0, 44, 44))
  var photosCollectionView:MABFlickyCollectionView = MABFlickyCollectionView(frame: CGRectZero, collectionViewLayout: MABFlickyLayout())
  var photosArray:[MABFlickrPhotoResult] = []
  var initialPoint:CGPoint = CGPointMake(0, 0)
  
  var canRotate:Bool = false
  var fetching:Bool = false
  var searchText:String = ""
  var nextPage:Int = 1
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view, typically from a nib.
    self.photosCollectionView.backgroundColor = UIColor.clearColor()
    self.photosCollectionView.registerClass(MABFlickyCollectionViewCell.self, forCellWithReuseIdentifier: "photoCell")
    self.photosCollectionView.collectionDataSource = self
    self.photosCollectionView.collectionDelegate = self
    
    self.searchView.backgroundColor = UIColor.whiteColor()
    self.searchView.barTintColor = UIColor.whiteColor()
    
    self.searchBar.delegate = self
    self.searchBar.searchBarStyle = UISearchBarStyle.Minimal
    self.searchBar.backgroundColor = UIColor.whiteColor()
    self.searchBar.tintColor = UIColor.blackColor()
    self.searchBar.showsCancelButton = true
    
    self.photosCollectionView.addSubview(self.loading)
    self.view.addSubview(self.photosCollectionView)
    self.view.addSubview(self.searchView)
    self.view.addSubview(self.searchBar)
    
    self.photosCollectionView.reloadData()
    
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  override func viewWillLayoutSubviews() {
    self.searchView.frame = CGRectMake(0, 0, self.view.bounds.size.width, 64)
    self.searchBar.frame = CGRectMake(0, 20, self.view.bounds.size.width, 44)
    self.photosCollectionView.frame = self.view.bounds
    self.loading.center = CGPointMake(self.photosCollectionView.frame.size.width / 2, self.photosCollectionView.contentSize.height - 25)
  }
  
  override func viewWillAppear(animated: Bool) {
    UIApplication.sharedApplication().statusBarStyle = UIStatusBarStyle.Default
    self.canRotate = false
  }
  
  
  override func shouldAutorotate() -> Bool {
    if (UIDevice.currentDevice().userInterfaceIdiom == UIUserInterfaceIdiom.Pad) {
      return true
    }
    else {
      if (self.canRotate) {
        return true
      }
      return false
    }
  }
  
  override func supportedInterfaceOrientations() -> Int {
    if (UIDevice.currentDevice().userInterfaceIdiom == UIUserInterfaceIdiom.Pad) {
      return Int(UIInterfaceOrientationMask.All.rawValue)
    }
    return Int(UIInterfaceOrientationMask.AllButUpsideDown.rawValue)
  }

  
  
  func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
    
    var cell = collectionView.dequeueReusableCellWithReuseIdentifier("photoCell", forIndexPath: indexPath) as MABFlickyCollectionViewCell
    if ((indexPath.row < self.photosArray.count) && (indexPath.row > -1)) {
      let photo = self.photosArray[indexPath.row]
      cell.loadPhotoWithURL(photo.thmbURL, andIndex: indexPath.row)
    }
    return cell
    
  }
  
  func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return self.photosArray.count
  }
  
  func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
    if let atts:UICollectionViewLayoutAttributes = self.photosCollectionView.layoutAttributesForItemAtIndexPath(indexPath) {
      self.initialPoint = CGPointMake(atts.frame.origin.x + (atts.frame.size.width / 2), 64 + atts.frame.origin.y + (atts.frame.size.height / 2) - self.photosCollectionView.contentOffset.y)
    }
    var vc = MABFlickyGalleryViewController()
    vc.photosArray = self.photosArray
    vc.currentIndex = indexPath.row
    vc.transitioningDelegate = self
    vc.modalPresentationStyle = UIModalPresentationStyle.Custom
    self.canRotate = true
    self.presentViewController(vc, animated: true, completion: {})
  }
  
  func scrollViewDidScroll(scrollView: UIScrollView) {
    self.loading.center = CGPointMake(self.photosCollectionView.frame.size.width / 2, self.photosCollectionView.contentSize.height - 25)
    if ((self.photosCollectionView.contentSize.height > self.photosCollectionView.frame.height) && (!self.fetching)) {
      if (self.photosCollectionView.contentOffset.y > (self.photosCollectionView.contentSize.height - self.photosCollectionView.frame.height - 25)) {
        self.resumeSearch()
      }
    }
  }
  
  
  
  func searchBarCancelButtonClicked(searchBar: UISearchBar) {
    searchBar.resignFirstResponder()
  }
  
  func searchBarSearchButtonClicked(searchBar: UISearchBar) {
    
    searchBar.resignFirstResponder()
    if (searchBar.text != "") {
      self.photosArray = []
      self.photosCollectionView.reloadData()
      self.searchText = searchBar.text
      self.nextPage = 1
      self.resumeSearch()
    }
    
  }
  
  func resumeSearch() {
    if (self.searchText != "") {
      self.loading.startAnimating()
      self.fetching = true
      MABFlickrAPIHandler.sharedHandler.searchPhotosContaining(self.searchText, perPage: 24,page: self.nextPage, completion: {(resultsArray:[MABFlickrPhotoResult], error:NSError!) in
        for var i = 0; i < resultsArray.count; i++ {
          self.photosArray.append(resultsArray[i])
        }
        self.nextPage++
        self.fetching = false
        self.loading.stopAnimating()
        self.photosCollectionView.reloadData()
        if (error != nil) {
          let alert = UIAlertView(title: "Error \(error.code)!", message: error.localizedDescription, delegate: nil, cancelButtonTitle: "Ok")
          alert.show()
        }
      })
    }
  }
  
  
  func animationControllerForPresentedController(presented: UIViewController, presentingController presenting: UIViewController, sourceController source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
    let animator = MABFlickyAnimator()
    animator.presenting = true
    animator.initialPoint = self.initialPoint
    return animator
  }
  
  func animationControllerForDismissedController(dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
    let animator = MABFlickyAnimator()
    return animator
  }
  
  
  
}

