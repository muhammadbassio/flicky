//
//  MABFlickyGalleryViewController.swift
//  Flicky
//
//  Created by Muhammad Bassio on 2/4/15.
//  Copyright (c) 2015 Muhammad Bassio. All rights reserved.
//

import UIKit


class MABFlickyAnimator: NSObject, UIViewControllerAnimatedTransitioning {
  
  var initialPoint:CGPoint = CGPointMake(0, 0)
  var presenting:Bool = false
  
  func transitionDuration(transitionContext: UIViewControllerContextTransitioning) -> NSTimeInterval {
    return 0.4
  }
  
  func animateTransition(transitionContext: UIViewControllerContextTransitioning) {
    
    if let fromVC = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey) {
      if let toVC = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey) {
        if (self.presenting) {
          toVC.view.frame = transitionContext.containerView().bounds
          // correction for iOS7
          let iosVersion = NSString(string: UIDevice.currentDevice().systemVersion).doubleValue
          if (iosVersion < 8) {
            let ct = fromVC.view.transform
            if (!CGAffineTransformIsIdentity(ct)) {
              if let superFrame = fromVC.view.superview?.frame {
                var rect = fromVC.view.frame
                var transOrigin = rect.origin
                transOrigin = CGPointApplyAffineTransform(transOrigin, ct)
                rect.origin = CGPointZero
                rect = CGRectApplyAffineTransform(rect, ct)
                if (rect.origin.x < 0.0) {
                  transOrigin.x = superFrame.size.width + rect.origin.x + transOrigin.x;
                }
                if (rect.origin.y < 0.0) {
                  transOrigin.y = superFrame.size.height + rect.origin.y + transOrigin.y;
                }
                rect.origin = transOrigin
                toVC.view.frame = rect
              }
            }
          }
          
          transitionContext.containerView().addSubview(toVC.view)
          toVC.view.layer.transform = CATransform3DMakeScale(0.001, 0.001, 0.001)
          toVC.view.center = initialPoint
          
          UIView.animateWithDuration(0.4, animations: {
            toVC.view.layer.transform = CATransform3DMakeScale(1.0, 1.0, 1.0)
            toVC.view.center = CGPointMake(fromVC.view.center.x, fromVC.view.center.y)
            }, completion: { (finished:Bool) in
              transitionContext.completeTransition(true)
          })
        }
        else {
          toVC.viewWillAppear(true)
          UIView.animateWithDuration(0.2, animations: {
            fromVC.view.layer.transform = CATransform3DMakeScale(0.3, 0.3, 0.3)
            }, completion: { (finished:Bool) in
              UIView.animateWithDuration(0.4, animations: {
                fromVC.view.center = CGPointMake(fromVC.view.center.x, -(transitionContext.containerView().bounds.size.height / 2))
                }, completion: { (finished:Bool) in
                  transitionContext.completeTransition(true)
              })
          })
        }
      }
    }
    
  }
  
  
  
  
}


class MABFlickyGalleryViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {
  
  var closeButton:UIButton = UIButton(frame: CGRectMake(0, 0, 70, 30))
  var mainScroller:UICollectionView = UICollectionView(frame: CGRectZero, collectionViewLayout: MABFlickyGalleryLayout())
  
  var photosArray:[MABFlickrPhotoResult] = []
  var currentIndex:Int = 0
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    // Do any additional setup after loading the view.
    self.view.backgroundColor = UIColor.blackColor()
    
    self.closeButton.backgroundColor = UIColor.blackColor()
    self.closeButton.clipsToBounds = true
    self.closeButton.layer.cornerRadius = 3
    self.closeButton.layer.borderColor = UIColor.whiteColor().CGColor
    self.closeButton.layer.borderWidth = 1
    self.closeButton.setTitle("Close", forState: .Normal)
    self.closeButton.setTitleColor(UIColor.whiteColor(), forState: .Normal)
    self.closeButton.setTitleColor(UIColor.whiteColor().colorWithAlphaComponent(0.6), forState: .Highlighted)
    self.closeButton.addTarget(self, action: "closePressed", forControlEvents: UIControlEvents.TouchUpInside)
    
    self.mainScroller.backgroundColor = UIColor.clearColor()
    self.mainScroller.registerClass(MABFlickyGalleryCell.self, forCellWithReuseIdentifier: "photoCell")
    self.mainScroller.dataSource = self
    self.mainScroller.delegate = self
    self.mainScroller.pagingEnabled = true
    
    self.view.addSubview(self.mainScroller)
    self.view.addSubview(self.closeButton)
    
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  override func viewWillLayoutSubviews() {
    self.closeButton.center = CGPointMake(self.view.bounds.size.width - 45, 45)
    self.mainScroller.frame = self.view.bounds
    self.mainScroller.contentOffset = CGPointMake(CGFloat(self.currentIndex) * self.mainScroller.frame.size.width, 0)
    self.mainScroller.reloadData()
    if (UIDevice.currentDevice().userInterfaceIdiom == UIUserInterfaceIdiom.Phone) {
      if (UIApplication.sharedApplication().statusBarOrientation.isLandscape) {
        self.closeButton.hidden = true
      }
      else {
        self.closeButton.hidden = false
      }
    }
  }
  
  override func viewWillAppear(animated: Bool) {
    UIApplication.sharedApplication().statusBarStyle = UIStatusBarStyle.LightContent
  }
  
  
  override func shouldAutorotate() -> Bool {
    return true
  }
  
  override func supportedInterfaceOrientations() -> Int {
    if (UIDevice.currentDevice().userInterfaceIdiom == UIUserInterfaceIdiom.Pad) {
      return Int(UIInterfaceOrientationMask.All.rawValue)
    }
    return Int(UIInterfaceOrientationMask.AllButUpsideDown.rawValue)
  }
  
  
  
  
  
  func closePressed() {
    UIApplication.sharedApplication().statusBarStyle = UIStatusBarStyle.Default
    self.dismissViewControllerAnimated(true, completion: {})
  }
  
  
  func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
    var cell = collectionView.dequeueReusableCellWithReuseIdentifier("photoCell", forIndexPath: indexPath) as MABFlickyGalleryCell
    if ((indexPath.row < self.photosArray.count) && (indexPath.row > -1)) {
      self.currentIndex = indexPath.row
      let photo = self.photosArray[indexPath.row]
      if (MABImageCache.sharedCache.isCached(photo.thmbURL)) {
        MABImageCache.sharedCache.loadImageForURL(photo.thmbURL, completionHandler: {(image:UIImage) in
          cell.loadPhotoWithURL(photo.imageURL, placeholderImage: image, andIndex: indexPath.row)
        })
      }
      else {
        cell.loadPhotoWithURL(photo.imageURL, andIndex: indexPath.row)
      }
    }
    return cell
  }
  
  func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return self.photosArray.count
  }
  
  /*
  // MARK: - Navigation
  
  // In a storyboard-based application, you will often want to do a little preparation before navigation
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
  // Get the new view controller using segue.destinationViewController.
  // Pass the selected object to the new view controller.
  }
  */
  
}
