//
//  MABActivityIndicatorView.swift
//  Tuber
//
//  Created by Muhammad Bassio on 12/30/14.
//  Copyright (c) 2014 Muhammad Bassio. All rights reserved.
//

import UIKit

class MABActivityIndicatorView: UIView {
  
  var circleLayer: CAShapeLayer! = CAShapeLayer()
  var rotationAnimation:CABasicAnimation! = CABasicAnimation(keyPath: "transform.rotation.z")
  
  let outlinePath: CGPath = {
    var path = CGPathCreateMutable()
    CGPathAddArc(path, nil, 19, 22, 12, CGFloat(M_PI_4/2), CGFloat((2*M_PI) + (M_PI_4/2)), false);
    CGPathAddLineToPoint(path, nil, 42, 35);
    return path
    }()
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    
    self.circleLayer.path = outlinePath;
    self.circleLayer.fillColor = UIColor.clearColor().CGColor
    self.circleLayer.strokeColor = UIColor.lightGrayColor().CGColor
    self.circleLayer.lineWidth = 2
    self.circleLayer.opacity = 0;
    self.circleLayer.strokeStart = 0.1;
    self.circleLayer.strokeEnd = 0.84;
    
    self.layer.addSublayer(circleLayer)
    
    self.rotationAnimation.toValue = CGFloat(M_PI) * 2.0;
    self.rotationAnimation.duration = 1;
    self.rotationAnimation.repeatCount = FLT_MAX;
    self.rotationAnimation.cumulative = false;
    self.rotationAnimation.removedOnCompletion = false;
    
  }
  
  required init(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder);
  }
  
  var color: UIColor = UIColor.grayColor() {
    didSet {
      self.circleLayer.strokeColor = self.color.CGColor
    }
  }
  
  override func layoutSubviews() {
    self.circleLayer.position = CGPointMake(20, 22)
    self.circleLayer.bounds = self.bounds
  }
  
  func startAnimating() {
    self.circleLayer.opacity = 1;
    self.circleLayer.addAnimation(rotationAnimation, forKey: "rotation");
  }
  
  func stopAnimating() {
    self.circleLayer.opacity = 0;
    self.circleLayer.removeAnimationForKey("rotation");
  }
  
}
